package com.felipe.factorypattern

class BreadFactory {
    fun getBread(breadType: String?): Bread? {
        if (breadType == null) return null

        return when (breadType) {
            "BAG" -> Baguette()
            "ROL" -> Roll()
            "SLI" -> Sliced()
            else -> null
        }
        /*
        if (breadType == "BAG")
            return Baguette()
        else if (breadType == "ROL")
            return Roll()
        else if (breadType == "SLI")
            return Sliced()
        */

    }
}