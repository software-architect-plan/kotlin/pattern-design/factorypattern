package com.felipe.factorypattern.homework

interface Movie {
    fun name(): String
    fun author(): String
}