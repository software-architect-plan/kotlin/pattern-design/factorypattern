package com.felipe.factorypattern.homework

class Titanic: Movie {
    override fun name(): String = "Titanic"

    override fun author(): String = "James Cameroon"
}