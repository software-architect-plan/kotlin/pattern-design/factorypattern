package com.felipe.factorypattern.homework

class Origin: Movie {
    override fun name(): String = "El Origin"

    override fun author(): String = "Christopher Nolan"
}