package com.felipe.factorypattern.homework

class PokemonFactory {
    fun getPokemon(pokemonType: String?): Pokemon? {
        if (pokemonType == null) return null
        return when (pokemonType) {
            "PIK" -> Pikachu()
            "CHAR" -> Charmander()
            "BUL" -> Bulbasaur()
            else -> null
        }

    }
}