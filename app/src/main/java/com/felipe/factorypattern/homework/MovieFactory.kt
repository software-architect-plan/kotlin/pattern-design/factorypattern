package com.felipe.factorypattern.homework

class MovieFactory {

    fun getMovie(movieType: String?): Movie? {
        if (movieType == null) return null

        return when (movieType) {
            "TIT" -> Titanic()
            "AVE" -> Avengers()
            "ORI" -> Origin()
            else -> null
        }

    }
}