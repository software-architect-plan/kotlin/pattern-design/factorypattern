package com.felipe.factorypattern.homework

interface Pokemon {
    fun name(): String
    fun skill(): String
}