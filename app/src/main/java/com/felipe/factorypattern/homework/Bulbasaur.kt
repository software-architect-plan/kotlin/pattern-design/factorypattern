package com.felipe.factorypattern.homework

class Bulbasaur: Pokemon {
    override fun name(): String = "Bulbasaur"

    override fun skill(): String = "Latigo cepa"

}