package com.felipe.factorypattern.homework

class Pikachu: Pokemon {
    override fun name(): String = "Pikachu"

    override fun skill(): String = "Impact trueno"
}