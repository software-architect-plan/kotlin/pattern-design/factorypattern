package com.felipe.factorypattern.homework

class Charmander: Pokemon {
    override fun name(): String = "Charmander"

    override fun skill(): String = "Fuego"
}