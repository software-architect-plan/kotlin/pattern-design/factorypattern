package com.felipe.factorypattern.homework

class Avengers: Movie {
    override fun name(): String = "Avengers"

    override fun author(): String = "Stan lee"
}